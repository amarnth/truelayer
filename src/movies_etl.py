from os import environ
from os.path import join, dirname, abspath
import logging
import time
import json
import pandas as pd

# 3rd party
import pyspark
from pyspark.sql import SQLContext
import pyspark.sql.functions as F
from pyspark.sql.types import (StructType, StructField, StringType,
                               IntegerType, DoubleType)
from pyspark.sql.functions import year
from sqlalchemy import create_engine
import glob

logger = logging.getLogger(__name__)

# Directory structure
base_dir = dirname(abspath(__file__))
data_dir = join(base_dir, "data")
jars_dir = join(base_dir, "jars")
psql_jar = join(jars_dir, "postgresql-42.2.9.jar")
environ['PYSPARK_SUBMIT_ARGS'] = \
    f"--packages com.databricks:spark-xml_2.10:0.4.1 " \
    f"--jars {psql_jar}  pyspark-shell"


def parse_json(data):
    if data and data.startswith("["):
        data = data.replace("'", "\"").replace("\\", "")
        return ', '.join([i["name"] for i in json.loads(data)])
    else:
        return None


def calculate_time(func):
    """ Just for calculating time """
    def inner1(*args, **kwargs):
        # storing time before function execution
        _begin = time.time()

        returned_value = func(*args, **kwargs)

        # storing time after function execution
        _end = time.time()
        print("Total time taken in : ", func.__name__, _end - _begin)
        return returned_value
    return inner1


class Connector:
    """ Database connector class """
    def __init__(self):
        self.db_url = f"jdbc:postgresql://{environ['host']}:" \
                      f"{environ['port']}/{environ['db_name']}"

        self.db_properties = {"user": environ['db_user'],
                              "password": environ['db_pwd'],
                              "driver": environ['driver']}
        self.engine = create_engine(f"postgresql://{environ['db_user']}:"
                                    f"{environ['db_pwd']}@{environ['host']}:"
                                    f"{environ['port']}/{environ['db_name']}")

        self.xml_schema = StructType([StructField("title", StringType(), True),
                                      StructField("url", StringType(), True),
                                      StructField("abstract", StringType(),
                                                  True)])
        self.metadata_schema = StructType([StructField("adult", StringType()),
                StructField("belongs_to_collection", StringType()),
                StructField("budget", IntegerType()),
                StructField("genres", StringType()),
                StructField("homepage", StringType()),
                StructField("id", IntegerType()),
                StructField("imdb_id", StringType()),
                StructField("original_language", StringType()),
                StructField("original_title", StringType()),
                StructField("overview", StringType()),
                StructField("popularity", StringType()),
                StructField("poster_path", StringType()),
                StructField("production_companies", StringType()),
                StructField("production_countries", StringType()),
                StructField("release_date", StringType()),
                StructField("revenue", IntegerType()),
                StructField("runtime", StringType()),
                StructField("spoken_languages", StringType()),
                StructField("status", StringType()),
                StructField("tagline", StringType()),
                StructField("title", StringType()),
                StructField("video", StringType()),
                StructField("vote_average", DoubleType()),
                StructField("vote_count", IntegerType())
                ])

        self.table_name = 'movies'


class Movies(Connector):
    def __init__(self):
        Connector.__init__(self)
        self.sc = pyspark.SparkContext('local[*]')
        self.sqlContext = SQLContext(self.sc)
        # _xml = join(data_dir, "enwiki-latest-abstract.xml")
        # _csv = join(data_dir, "movies_metadata.csv")
        _xml = glob.glob(join(data_dir, "*.xml"))[0]
        _csv = glob.glob(join(data_dir, "*.csv"))[0]

        self.df = self.sqlContext.read.format(
            "com.databricks.spark.xml").options(rowTag="doc")\
            .load(_xml, schema=self.xml_schema)

        df_movies = self.sqlContext.read.csv(_csv, header=True,
                                             encoding="UTF-8",
                                             schema=self.metadata_schema)
        df_movies = self.format_metadata(df_movies)

        # user defined functions
        self._udf = F.udf(lambda x: x.split(": ")[1], StringType())
        self.format_wiki()
        self.join_data(df_movies)
        self.dump_using_spark_df()
        # self.convert_to_pandas()
        # self.dump_to_database()
        self.psql_select()

    def format_wiki(self):
        self.df = self.df.select(self._udf(self.df['title']).alias("title"),
                                 self.df['url'].alias("wiki_url"),
                                 self.df['abstract'].alias("wiki_abstract"))

    @staticmethod
    def format_metadata(df_m):
        udf_json = F.udf(lambda x: parse_json(x), StringType())
        df_m = df_m.select("title", "budget", "revenue",
                           year(df_m["release_date"]).alias('year'),
                           df_m["vote_average"].alias('rating'),
                           udf_json('genres').alias("genres"),
                           udf_json('production_companies').alias(
                               'production_companies'),
                           (df_m["revenue"] / df_m["budget"]).cast(
                               "double").alias("ratio"))
        return df_m

    def join_data(self, df_movies):
        self.df = self.df.join(df_movies, "title", "inner")
        self.df = self.df.orderBy("ratio", ascending=False).limit(5000)

    @calculate_time
    def convert_to_pandas(self):
        print("converting to pandas")
        self.df = self.df.toPandas()

    @calculate_time
    def dump_to_database(self):
        self.df.to_sql(name=self.table_name, con=self.engine,
                       if_exists='append', index=False)

    @calculate_time
    def dump_using_spark_df(self):
        self.df.write.jdbc(url=self.db_url, table=self.table_name,
                           mode='overwrite', properties=self.db_properties)

    @calculate_time
    def psql_select(self):
        res_df = pd.read_sql('SELECT * FROM movies limit10', self.engine)
        print(res_df.head())


if __name__ == '__main__':
    begin = time.time()
    Movies()
    end = time.time()
    print("Total time taken: {0}".format(end - begin))
