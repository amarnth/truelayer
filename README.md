## prerequisite
Docker should be up and running

NOTE: Please make sure you have the XML and CSV file in 

*src/data/* directory

# How to run my code
```bash 
make run 
```

## Additional informations
### pyspark
Used PySpark since, it runs in memory and distributed in a cluster.

### Data Structure
Import the data to psql can be done in many ways, tried with couple of the best ways to do it.

1. Converting the pyspark dataframe into pandas daraframe 
2. Use pyspark dataframe directly insert into psql

### Docker
Easy to spin up the postgres docker image and for the database and connections

#### Test Case
Run UnitTestCase

Query the table and check with the expected result.

