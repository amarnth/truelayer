.PHONY: run
.PHONY: test
.ONESHELL:

guard-%:
	@ if [ "${${*}}" = "" ]; then echo "Environment variable $* not set"; exit 1; fi

export db_user ?= dev
export db_pwd ?= root
export host ?= localhost
export port ?= 5432
export db_name ?= etl
export driver ?= org.postgresql.Driver
# driver must exists before run

activate:
	rm -rf ./venv/
	test -f ./venv/bin/activate || virtualenv -p $(shell which python3) venv
	source ./venv/bin/activate ;\
	pip install -r src/requirements.txt ;\
	touch ./venv/bin/activate

run:
	make activate ; \
	docker-compose -f docker-compose.yml up -d --no-recreate -t 1200 ; \
	cd src ; python movies_etl.py